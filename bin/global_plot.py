"""
Plot a stat over another stat.

Example:
    python plot.py --inputfolder simData/numMotes_50/ -x chargeConsumed --y aveLatency
"""

# =========================== imports =========================================

# standard
import os
import argparse
import json
import glob
from collections import OrderedDict
import numpy as np
import pytabular as pytab

# third party
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

# ============================ defines ========================================

KPIS = [
    'e2e-upstream-delivery',
    'latency_avg_s',
    'network_lifetime',
    'motes_out_of_scheduler',
    'data_collision'
]

# KPIS = [
#     'data_collision',
# ]

# ============================ main ===========================================

def create_table(data, key, subfolder):
    nMotes = []

    for km in data.keys():
        for dbm in data[km].keys():
            for sf in data[km][dbm].keys():
                nMotes = data[km][dbm][sf].keys()
    
    mHeader = ['']
    pHeader = ['']
    for i in nMotes:
        mHeader = mHeader + [i, '', '', '', '']
        pHeader = pHeader + ['Min', 'Q1', 'Mediana', 'Q3', 'Max']
    
    
    headers = [mHeader, pHeader]


    for km in data.keys():
        for dbm in data[km].keys():
            dt = []    
            for sf in data[km][dbm].keys():
                aux = [sf]
                for nuMmotes in  data[km][dbm][sf].keys():
                    vals = data[km][dbm][sf][nuMmotes]
                    Q1, median, Q3 = np.percentile(vals, [25, 50, 75])
                    IQR = Q3 - Q1

                    loval = Q1 - 1.5 * IQR
                    hival = Q3 + 1.5 * IQR

                    wiskhi = np.compress(vals <= hival, vals)
                    wisklo = np.compress(vals >= loval, vals)
                    actual_hival = np.max(wiskhi)
                    actual_loval = np.min(wisklo)
                    
                    actual_loval = round((actual_loval * 100), 2) / 100
                    actual_hival = round(actual_hival * 100, 2) / 100
                    Q1 = round(Q1 * 100, 2) / 100
                    Q3 = round(Q3 * 100, 2) / 100
                    median = round(median * 100, 2) / 100

                    aux = aux + [actual_loval, Q1, median, Q3, actual_hival]
                dt = dt + [aux]

            dt = headers + dt
            tabular = pytab.Tabular(dt)
            f = open( subfolder + '/{0}km_{1}dbm/{2}-table.tex'.format(km, dbm, key), 'w+')
            f.write(str(tabular))
            f.close()

def set_box_color(bp, color):
    plt.setp(bp['boxes'], color=color)
    plt.setp(bp['whiskers'], color=color)
    plt.setp(bp['caps'], color=color)
    plt.setp(bp['medians'], color=color)

def main(options):

    # init
    

    subfolder = options.inputfolder

    for key in options.kpis:
        # load data
        data = OrderedDict()
        for file_path in sorted(glob.glob(os.path.join(subfolder, '*.kpi'))):
            curr_combination = os.path.basename(file_path)[:-8] # remove .dat.kpi
            with open(file_path, 'r') as f:

                # read kpi file
                kpis = json.load(f)

                # init data list
                params = curr_combination.split("_")
                sf = params[2]
                if sf == "FRSF":
                    sf = "CCRSF"
                numMotes = params[5]
                km = params[7]
                dbm = params[9]

                
                if km not in data.keys():
                    data[km] = OrderedDict()
                
                if dbm not in data[km].keys():
                    data[km][dbm] = OrderedDict()
                
                if sf not in data[km][dbm].keys():
                    data[km][dbm][sf] = OrderedDict()

                if numMotes not in data[km][dbm][sf].keys():
                    data[km][dbm][sf][numMotes] = []
                


                # fill data list
                for run in kpis.itervalues():
                    
                    if key in run['global-stats']:
                        if "total" in run['global-stats'][key][0].keys():
                            data[km][dbm][sf][numMotes].append(run['global-stats'][key][0]["total"])
                        elif "value" in run['global-stats'][key][0].keys():
                            data[km][dbm][sf][numMotes].append(run['global-stats'][key][0]["value"])
                        elif "min" in run['global-stats'][key][0].keys():
                            data[km][dbm][sf][numMotes].append(run['global-stats'][key][0]["min"])
                    for mote in run.itervalues():
                        if mote is run['global-stats']:
                            continue

                        if key in mote:
                            data[km][dbm][sf][numMotes].append(mote[key])


        
        sorted_data = OrderedDict()
        for km in data.keys():
            sorted_data[km] = OrderedDict()
            for dbm in data[km].keys():
                sorted_data[km][dbm] = OrderedDict()
                for sf in data[km][dbm].keys():
                    sorted_x = sorted(data[km][dbm][sf].items(), key= lambda x: x[0])
                    sorted_data[km][dbm][sf] = OrderedDict(sorted_x)

        
        # plot
        
        
        try:

            plot_box(sorted_data, key, subfolder)
            plot_lines(sorted_data, key, subfolder)
             

            if key in ['lifetime_AA_years', 'latencies']:
                plot_cdf(sorted_data, key, subfolder)
            
            create_table(sorted_data, key, subfolder) 
                
                

        except TypeError as e:
            print "Cannot create a plot for {0}: {1}.".format(key, e)
    print "Plots are saved in the {0} folder.".format(subfolder)

# =========================== helpers =========================================

def plot_cdf(data, key, subfolder):
    plt.figure(figsize=(8, 6))
    color_schemes = [plt.cm.Blues(np.linspace(0,1,20)), plt.cm.Reds(np.linspace(0,1,20)), plt.cm.Greens(np.linspace(0,1,20))]
    
    for km in data.keys():
        
        
        for dbm in data[km].keys():
            current_scheme = 0
            current_color = 10
            for sf in data[km][dbm].keys():
                colors = color_schemes[current_scheme]
                max_motes = max(data[km][dbm][sf].keys())
                 
                if type(data[km][dbm][sf][max_motes][0]) == list:
                    values = sum(data[km][dbm][sf][max_motes], [])
                else:
                    values = data[km][dbm][sf][max_motes]
                # compute CDF
                sorted_data = np.sort(values)
                yvals = np.arange(len(sorted_data)) / float(len(sorted_data) - 1)
                plt.plot(sorted_data, yvals, label= sf, color=colors[current_color])
                current_scheme += 1

            plt.xlabel(key)
            plt.ylabel("CDF")
            plt.legend(ncol = 1, loc='center left', bbox_to_anchor=(1, 0.5))
            
            savefig(subfolder + "/{0}km_{1}dbm".format(km, dbm), key + ".cdf")
            plt.clf()


def plot_box(data, key, subfolder):
    plt.figure(figsize=(8, 6))
    color_schemes = [plt.cm.Blues(np.linspace(0,1,20)), plt.cm.Reds(np.linspace(0,1,20)), plt.cm.Greens(np.linspace(0,1,20))]
    
    for km in data.keys():
        
        for dbm in data[km].keys():
            current_scheme = 0
            current_color = 10
            pad = -0.2
            for sf in data[km][dbm].keys():
                colors = color_schemes[current_scheme]
                positions = [float(i) + pad for i in range(len(data[km][dbm][sf].keys()))]
                label = sf
                bp = plt.boxplot(data[km][dbm][sf].values(), sym = '', labels=data[km][dbm][sf].keys(), positions = positions, widths = 0.1, notch= True )
                set_box_color(bp, colors[current_color])
                plt.plot([], label=label, color= colors[current_color])
                current_scheme += 1
                pad += 0.2

            if key == 'data_collision':
                plt.ylim(0, 1000)
            elif key in ['network_lifetime', 'latency_avg_s']:
                plt.ylim(0, 10)
            elif key == 'e2e-upstream-delivery':
                plt.ylim(0.88, 1)
            elif key == 'motes_out_of_scheduler':
                plt.ylim(0, 0.1)
            plt.margins(y=0.05)
            plt.ylabel(key)    
            plt.legend()
            plt.xlabel("Number of motes")
            name = key + "_box"
            savefig(subfolder + "/{0}km_{1}dbm".format(km, dbm), name)
            plt.clf()


def plot_lines(data, key, subfolder):
    
    plt.figure(figsize=(8, 6))
    color_schemes = [plt.cm.Blues(np.linspace(0,1,20)), plt.cm.Reds(np.linspace(0,1,20)), plt.cm.Greens(np.linspace(0,1,20))]
    
    for km in data.keys():
        
        for dbm in data[km].keys():
            current_scheme = 0
            current_color = 10
            for sf in data[km][dbm].keys():
                x = data[km][dbm][sf].keys()
                colors = color_schemes[current_scheme]
                label = sf
                y = []
                for numMotes in data[km][dbm][sf].keys():
                    median = np.median(data[km][dbm][sf][numMotes])
                    y.append(median)
                
                plt.plot(x, y, label=label, color= colors[current_color])
                current_scheme += 1
        
            plt.ylabel(key)    
            plt.legend()
            plt.xlabel("Number of motes")
            name = key + "_median"
            savefig(subfolder + "/{0}km_{1}dbm".format(km, dbm), name)
            plt.clf()

    

def savefig(output_folder, output_name, output_format="png"):
    # check if output folder exists and create it if not
    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)

    # save the figure
    plt.savefig(
        os.path.join(output_folder, output_name + "." + output_format),
        bbox_inches     = 'tight',
        pad_inches      = 0,
        format          = output_format,
    )

def parse_args():
    # parse options
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--inputfolder',
        help       = 'The simulation result folder.',
        default    = 'validSimData',
    )
    parser.add_argument(
        '-k','--kpis',
        help       = 'The kpis to plot',
        type       = list,
        default    = KPIS
    )
    parser.add_argument(
        '--xlabel',
        help       = 'The x-axis label',
        type       = str,
        default    = None,
    )
    parser.add_argument(
        '--ylabel',
        help       = 'The y-axis label',
        type       = str,
        default    = None,
    )
    parser.add_argument(
        '--show',
        help       = 'Show the plots.',
        action     = 'store_true',
        default    = None,
    )
    return parser.parse_args()

if __name__ == '__main__':

    options = parse_args()

    main(options)
