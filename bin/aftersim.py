import os
import argparse
import json
import glob
from collections import OrderedDict


def main(options):

    subfolders = list(
            map(lambda x: os.path.join(options.inputfolder, x),
                os.listdir(options.inputfolder)
            )
        )
    subfolder = max(subfolders, key=os.path.getmtime)
    subfolder_name = str(subfolder).split('/')[1]
    clear_dat_files = 'rm {0}/*.dat'.format(subfolder)
    config_file_path = '{0}/config.json'.format(subfolder)
    square_size = 0
    new_path = ''
    with open(config_file_path, 'r') as f:
        config = json.load(f)
        square_size = config['settings']['regular']['conn_random_square_side']
    
    new_path = './validSimData/'
    move_to_new_path = 'mv {0}/*.kpi {1}'.format(subfolder, new_path)

    print('Deleting .DAT files from simulation {0}'.format(subfolder))
    os.system(clear_dat_files)
    print('Renaming .dat.kpi files for global_plot.py')
    files = os.listdir(subfolder)
    for file in files:
        spl = str(file).split('.')
        if len(spl) > 2:
            new_filename = spl[0] + '_km_' + str(square_size) + '_dbm_' + options.db + '.{0}.{1}'.format(spl[1], spl[2])
            os.rename(os.path.join(subfolder, file), os.path.join(subfolder, new_filename))  
    
    print('Moving {0} KPIs to {1}'.format(subfolder, new_path))
    os.system('mkdir -p ./{0}'.format(new_path))
    os.system(move_to_new_path)
    print("Ready!")





            



def parse_args():
    # parse options
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--inputfolder',
        help       = 'The simulation result folder.',
        default    = 'simData',
    )
    parser.add_argument(
        '--db',
        help       = 'absolute dB level of tx power',
        default    = '0',
    )
    
    return parser.parse_args()

if __name__ == '__main__':

    options = parse_args()

    main(options)


