import os
import sys
import matplotlib.pyplot as plt
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
import json
import glob

if __name__ == '__main__':
    here = sys.path[0]
    sys.path.insert(0, os.path.join(here, '..'))

from SimEngine import SimLog

def openfile(func):
    def inner(inputfile):
        with open(inputfile, 'r') as f:
            return func(f)
    return inner

@openfile
def plot_network(infile):
    plots = {}
    print 'generating topology map for {0}'.format(infile.name)
    run_id = -1
    line_n = 0
    for line in infile:      
        logline = json.loads(line)
        run_id = logline['_run_id']
        if logline['_type'] != SimLog.LOG_CONNECTIVITY_TOPOLOGY['type']:
            continue
        else:
            motes = logline['_motes']
            neighbors = logline['_neighbors']
            G = nx.DiGraph()
            for _id, mote in enumerate(motes):
                coordinates = mote[str(_id)] 
                coordinates = [coordinate * 30 for coordinate in coordinates]

                G.add_node(_id)
                for neighbor in neighbors[str(_id)]:
                    G.add_edge(_id, neighbor);
            plots[run_id] = G
    
    assert(run_id != -1)
    
    for run_id in plots.keys():
        current_topology = plots[run_id]
        plt.subplot()
        # pos = nx.get_node_attributes(current_topology, 'pos')
            
        pos = nx.spring_layout(G, k=0.5,iterations=20);
        nx.draw(current_topology,node_color='green', with_labels=True, pos=pos)
        plt.savefig("./{0}_{1}_topology.png".format(infile.name[:-4], run_id))
        plt.close()


def main():
    subfolders = list(
        map(
            lambda x: os.path.join('simData', x),
            os.listdir('simData')
        )
    )
    # indexed by run_id
    
    subfolder = max(subfolders, key=os.path.getmtime)
    print(subfolder)
    for infile in glob.glob(os.path.join(subfolder, '*.dat')):
        plot_network(infile)
        
if __name__ == "__main__":
    main()

            
            




            