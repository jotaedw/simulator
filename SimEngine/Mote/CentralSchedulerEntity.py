import SimEngine

class CellReuseCentralSchedulerEntity(object):
    
    # == Init
    def __init__(self):
        
        self.setting = None
        self.log = None
        self.motes = {}
        self.scheduler = {}
        self.tx_blacklist = {}
        self.txrx_blacklist = {}
        self.dagRoot = None
        

    # == Private
   
    # == Parent handlers
    def _set_parent(self, mote, parent):
        self.motes[mote]["parent"] = parent
    
    def _get_parent(self, mote):
        return self.motes[mote]["parent"]
    
    # == Offspring handlers
    def _add_offspring(self, mote, offspring):
        self.motes[mote]["offsprings"].append(offspring)
    
    def _remove_offspring(self, mote, offspring):
        if offspring in self.motes[mote]["offsprings"]:
            self.motes[mote]["offsprings"].remove(offspring)
    
    def _get_offsprings(self, mote):
        return self.motes[mote]["offsprings"]
    
    # == Neighbor handlers
    def _add_neighbor(self, mote, neighbor):
        self.motes[mote]["neighbors"].append(neighbor)
    
    def _remove_neighbor(self, mote, neighbor):
        if neighbor in self.motes[mote]["neighbors"]:
            self.motes[mote]["neighbors"].remove(neighbor)
    
    def _get_neighbors(self, mote):
        
        return self.motes[mote]["neighbors"]
    

    # == Deph handlers
    def _set_deph(self, mote):
        if self.motes[mote]["parent"] is None:
            self.motes[mote]["deph"] = 0
        else:
            parent = self.motes[mote]["parent"]
            parent_deph = self.motes[parent]["deph"]
            self.motes[mote]["deph"] = parent_deph + 1

        mote_offsprings = self.motes[mote]["offsprings"]
        for offspring in mote_offsprings:
            self._set_deph(offspring)

    def _get_deph(self, mote):
        return self.motes[mote]["deph"]

    def _get_max_deph(self):
        return max(d["deph"] for d in self.motes.values())
   
    def _get_motes_by_deph(self, deph):
        ret_val = []
        for mote_mac, mote in self.motes.items():
            if mote["deph"] == deph:
                ret_val.append(mote_mac)
        
        return ret_val

    def _update_deph(self):
        dagRoot = self.dagRoot
        self._set_deph(dagRoot)
        

    # == Cells helper
    def _add_cell(self, src, dest, channel_offset, slot_offset, option):
        new_cell = {
            "channelOffset": channel_offset,
            "slotOffset": slot_offset,
            "neighbor": dest,
            "cellOption": option
        }
        self.motes[src]["cells"].append(new_cell)
    
    def _get_cells(self, mote):
        return self.motes[mote]["cells"]

    def _get_cells_by_mac(self, mote, mac):
        mote_cells = self.get_cells(mote)
        filtered_cells = filter(lambda cell: cell["mac"] == mac, mote_cells)
        return filtered_cells
    
    def _remove_cells_from_mote(self, mote, cells):
        for cell in cells:
            self.motes[mote]["cells"] = []
    
    def _clear_cells(self):
        for mote in self.motes.keys():
            self.motes[mote]["cells"] = []

    # == TX Blacklist handlers
    def _init_tx_blacklist(self):
        for channel_offset in range(self.settings.phy_numChans):
            for slot_offset in range(self.settings.tsch_slotframeLength):
                if (channel_offset, slot_offset) == (0,0):
                    continue
                self.tx_blacklist[(channel_offset, slot_offset)] = []

    def _tx_blacklist_neighbors(self, channel_offset, slot_offset, mote):
        current = self.tx_blacklist[(channel_offset, slot_offset)]
        neighbors = self._get_neighbors(mote)
        new_set = set(neighbors + current)
        self.tx_blacklist[(channel_offset, slot_offset)] = list(new_set)
    
    def _is_in_tx_blacklist(self, channel_offset, slot_offset, mote):
        blacklist_ids = [self.motes[mac]['mote'].id for mac in self.tx_blacklist[(channel_offset, slot_offset)]]
        self._log(
            SimEngine.SimEngine.SimLog.LOG_CSE_TX_BLACKLIST,
            {
                '_mote_id': self.motes[mote]['mote'].id,
                'tx_blacklist': blacklist_ids
            }
        )
        if mote in self.tx_blacklist[(channel_offset, slot_offset)]:
            return True
        else:
            return False
    
    # == TXRX Blacklist handlers
    def _init_txrx_blacklist(self):
        for channel_offset in range(self.settings.phy_numChans):
            for slot_offset in range(self.settings.tsch_slotframeLength):
                if (channel_offset, slot_offset) == (0,0):
                    continue
                self.txrx_blacklist[(channel_offset, slot_offset)] = []
                
    def _txrx_blacklist_neighbors(self, channel_offset, slot_offset, mote):
        current = self.txrx_blacklist[(channel_offset, slot_offset)]
        
        neighbors = self._get_neighbors(mote)
        new_set = set(neighbors + current)
        self.txrx_blacklist[(channel_offset, slot_offset)] = list(new_set)
    
    def _is_in_txrx_blacklist(self, channel_offset, slot_offset, mote):
        blacklist_ids = [self.motes[mac]['mote'].id for mac in self.txrx_blacklist[(channel_offset, slot_offset)]] 
        self._log(
            SimEngine.SimEngine.SimLog.LOG_CSE_TXRX_BLACKLIST,
            {
                '_mote_id': self.motes[mote]['mote'].id,
                'txrx_blacklist': blacklist_ids
            }
        )
        if mote in self.txrx_blacklist[(channel_offset, slot_offset)]:
            return True
        else:
            return False
    
    # == General handlers
    def _sort_motes_by_deph(self):
        sorted_motes = sorted(self.motes.items(), key = lambda k: k[1]["deph"])
        sorted_motes.reverse()
        sorted_keys = []
        sorted_ids = []
        for mote in sorted_motes:
            sorted_keys.append(mote[0])
            sorted_ids.append(mote[1]['mote'].id)
        self._log(
            SimEngine.SimEngine.SimLog.LOG_CSE_SORTED_MOTES,
            {
                '_motes_by_deph': sorted_ids
            }
        )
        return sorted_keys

    def _log(self, type, content):
        if self.dagRoot is not None:
            self.motes[self.dagRoot]['mote'].log(type, content)
        pass
    
    # == locked slot handlers
    def _clear_locked_slots(self):
        for mote_mac in self.motes.keys():
            self.motes[mote_mac]["locked_slots"] = set([])
        
    def _lock_slot(self, mote_mac, slot):
        self.motes[mote_mac]["locked_slots"].add(slot)

    def _get_locked_slots(self, mote_mac):
        return self.motes[mote_mac]["locked_slots"]
    
    def _is_slot_locked(self, mote1, mote2, slot):
        mote1_locked_slots = self._get_locked_slots(mote1)
        mote2_locked_slots = self._get_locked_slots(mote2)

        self._log(
            SimEngine.SimEngine.SimLog.LOG_CSE_LOCKED_SLOTS,
            {
                '_mote_id': self.motes[mote1]['mote'].id,
                'locked_slots': list(mote1_locked_slots)
            }
            
        )

        self._log(
            SimEngine.SimEngine.SimLog.LOG_CSE_LOCKED_SLOTS,
            {
                '_mote_id': self.motes[mote2]['mote'].id,
                'locked_slots': list(mote2_locked_slots)
            }
            
        )
        
        if slot in mote1_locked_slots or slot in mote2_locked_slots:
            return True
        
        return False


    # == Num cell TX handlers
    def _init_num_tx(self):
        for mote in self.motes.keys():
            self.motes[mote]["num_tx_cells"] = 1

    def _set_num_tx_cells(self):
        self._init_num_tx()
        max_deph = self._get_max_deph()
        for deph in range(max_deph, 0, -1):
            deph_motes = self._get_motes_by_deph(deph)
            for mote_mac in deph_motes:
                num_tx = self.motes[mote_mac]["num_tx_cells"]
                mote_offsprings = self._get_offsprings(mote_mac)
                for offspring_mac in mote_offsprings:
                    num_tx = num_tx + self.motes[offspring_mac]["num_tx_cells"]
                
                self._log(
                    SimEngine.SimEngine.SimLog.LOG_CSE_NUM_TX,
                    {
                        '_mote_id': self.motes[mote_mac]['mote'].id,
                        'num_tx': num_tx
                    }
                )
                self.motes[mote_mac]["num_tx_cells"] = num_tx

    def _get_num_tx(self, mote):
        return self.motes[mote]["num_tx_cells"]


    # == Scheduler handlers
    def _init_scheduler(self):
        for channel_offset in range(self.settings.phy_numChans):
            for slot_offset in range(self.settings.tsch_slotframeLength):
                if (channel_offset, slot_offset) == (0,0):
                    continue
                self.scheduler[(channel_offset, slot_offset)] = []

    def _add_to_scheduler(self, src, dest, channel_offset, slot_offset):
        new_cell = {
            "src": src,
            "dest": dest
        }
        self.scheduler[(channel_offset, slot_offset)].append(new_cell)
    
    def _set_scheduled_cells(self):
        for pair in self.scheduler.keys():
            for cell in self.scheduler[pair]:
                self._add_cell(cell["src"], cell["dest"], pair[0], pair[1], ['TX'])
                self._add_cell(cell["dest"], cell["src"], pair[0], pair[1], ['RX'])
            
    def _set_scheduler(self):

        self._init_scheduler()
        self._init_txrx_blacklist()
        self._init_tx_blacklist()
        self._update_deph()
        self._set_num_tx_cells()
        self._clear_locked_slots()
        
        sorted_motes = self._sort_motes_by_deph()
        for mote_mac in sorted_motes:
    
    
            mote = self.motes[mote_mac]           
            current_cells = self._get_cells(mote_mac)
            mote["mote"].sf.delete_cells(current_cells)
            self._remove_cells_from_mote(mote_mac, current_cells)
            mote_offsprings = self._get_offsprings(mote_mac)
            for offspring_mac in mote_offsprings:
        
                offspring = self.motes[offspring_mac]
                offspring_cells = self._get_cells(offspring_mac)
                offspring["mote"].sf.delete_cells(offspring_cells)
                self._remove_cells_from_mote(offspring_mac, offspring_cells)
                offspring_neighbors = offspring["neighbors"]
                added_cells = 0
                num_tx_cells = offspring["num_tx_cells"]
        
                for _ in range(num_tx_cells):
                    for channel_offset in range(self.settings.phy_numChans):
                        should_brake = False
                        for slot_offset in range(1, self.settings.tsch_slotframeLength):
                            
                            self._log(
                                SimEngine.SimEngine.SimLog.LOG_CSE_CURRENT_STATE,
                                {
                                    'num_tx': '{0}/{1}'.format(_ + 1, num_tx_cells),
                                    'cell': (channel_offset, slot_offset),
                                    'offspring': offspring['mote'].id,
                                    'parent': mote['mote'].id
                                }
                            )
                            offspring_in_tx_blacklist = self._is_in_tx_blacklist(channel_offset, slot_offset, offspring_mac)
                            offspring_in_txrx_blacklist = self._is_in_txrx_blacklist(channel_offset, slot_offset, offspring_mac)
                            parent_in_txrx_blacklist = self._is_in_txrx_blacklist(channel_offset, slot_offset, mote_mac)
                            locked_slot = self._is_slot_locked(mote_mac, offspring_mac, slot_offset)
                            
                            if offspring_in_tx_blacklist or offspring_in_txrx_blacklist or parent_in_txrx_blacklist or locked_slot:
                                continue
                            
                            else:
                                should_brake = True
                                self._add_to_scheduler(offspring_mac, mote_mac, channel_offset, slot_offset)
                                self._txrx_blacklist_neighbors(channel_offset, slot_offset, offspring_mac)
                                self._tx_blacklist_neighbors(channel_offset, slot_offset, mote_mac)
                                self._lock_slot(mote_mac, slot_offset)
                                self._lock_slot(offspring_mac, slot_offset)
                        
                                added_cells += 1
                                break
                            
                        if should_brake:
                            break
                        else:
                            continue
                
                if added_cells < num_tx_cells:
                    self.motes[offspring_mac]["mote"].sf.log_scheduler_full()
                else:
            
                    continue


        self._calc_cells()

    def _calc_cells(self):
        
        self._set_scheduled_cells()
        
        sorted_motes = self._sort_motes_by_deph()
        
        for mote_mac in sorted_motes:
            mote = self.motes[mote_mac]["mote"]
            mote_cells = self._get_cells(mote_mac)
            mote.sf.add_cells(mote_cells)

    # == Public

    def update_parent(self, mote, new_parent, old_parent):
        old_parent_log = None
        self._set_parent(mote, new_parent)
        
        if old_parent:
            old_parent_log = self.motes[old_parent]['mote'].id
            self._remove_offspring(old_parent, mote)
        
        if new_parent:
            self._add_offspring(new_parent, mote)
            self._set_scheduler()
            self._log(
                SimEngine.SimEngine.SimLog.LOG_CSE_UPDATE_PARENT,
                {
                    '_mote_id': self.motes[mote]['mote'].id,
                    'old_parent': old_parent_log,
                    'new_parent': self.motes[new_parent]['mote'].id
                }
            )

    def update_neighbors(self, mote, neighbor):
        if neighbor is not None:
            self._log(
                SimEngine.SimEngine.SimLog.LOG_CSE_UPDATE_NEIGHBORS,
                {
                    '_mote_id': self.motes[mote]['mote'].id,
                    'new_neighbor': self.motes[neighbor]['mote'].id
                }
            )
            self._add_neighbor(mote, neighbor)        
        
        # self._set_scheduler()
         
    def add_mote(self, mote, settings, log):
        self._log(
            SimEngine.SimEngine.SimLog.LOG_CSE_ADD_MOTE,
            {
                '_mote_id': mote.id
            }
        )
        if mote.id == 0:
            self.__init__()
            self.dagRoot = mote.get_mac_addr()

        mote_mac = mote.get_mac_addr()
        mote_parent = mote.rpl.getPreferredParent()
        new_mote = {
            "mote": mote,
            "parent": mote_parent,
            "offsprings": [],
            "cells": [],
            "neighbors": [],
            "num_tx_cells": 1,
            "deph": -1,
            "locked_slots": set([])
        }
        self.settings = settings
        self.log = log
        self.motes[mote_mac] = new_mote

    def remove_mote(self, mote_mac):

        self._log(
            SimEngine.SimEngine.SimLog.LOG_CSE_REMOVE_MOTE,
            {
                '_mote_id': self.motes[mote_mac]['mote'].id
            }
        )

        mote_neighbors = self._get_neighbors(mote_mac)
        mote_offsprings = self._get_offsprings(mote_mac)
        mote_parent = self._get_parent(mote_mac)
        for neighbor_mac in mote_neighbors:
            self._remove_neighbor(neighbor_mac, mote_mac)
        
        for offspring_mac in mote_offsprings:
            self._set_parent(offspring_mac, None)
        
        if mote_parent is not None:
            self._remove_offspring(mote_parent, mote_mac)
        
        del self.motes[mote_mac]



class StandarCentralSchedulingEntity(CellReuseCentralSchedulerEntity):
    
     # == TX Blacklist handlers
    def _init_tx_blacklist(self):
        pass

    def _tx_blacklist_neighbors(self, channel_offset, slot_offset, mote):
        pass
    
    def _is_in_tx_blacklist(self, channel_offset, slot_offset, mote):
        return False
    
    # == TXRX Blacklist handlers
    def _init_txrx_blacklist(self):
        pass

    def _txrx_blacklist_neighbors(self, channel_offset, slot_offset, mote):
        pass
    
    def _is_in_txrx_blacklist(self, channel_offset, slot_offset, mote):
        return False
    